package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/brunobandev/grapecheck/helpers"
	"github.com/brunobandev/grapecheck/models"

	"github.com/gorilla/mux"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func getWines(w http.ResponseWriter, r *http.Request) {
	var wines []models.Wine
	collection := helpers.ConnectDB()
	cur, err := collection.Find(context.TODO(), bson.M{})

	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {
		var wine models.Wine
		err := cur.Decode(&wine)
		if err != nil {
			log.Fatal(err)
		}

		wines = append(wines, wine)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	respondWithJson(w, http.StatusOK, wines)
}

func getWine(w http.ResponseWriter, r *http.Request) {
	var wine models.Wine
	var params = mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(params["id"])

	collection := helpers.ConnectDB()

	filter := bson.M{"_id": id}
	err := collection.FindOne(context.TODO(), filter).Decode(&wine)

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Wine ID")
		return
	}

	respondWithJson(w, http.StatusOK, wine)
}

func createWine(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var wine models.Wine

	err := json.NewDecoder(r.Body).Decode(&wine)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	collection := helpers.ConnectDB()
	result, err := collection.InsertOne(context.TODO(), wine)

	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJson(w, http.StatusCreated, result)
}

func updateWine(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var params = mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(params["id"])
	var wine models.Wine

	collection := helpers.ConnectDB()
	filter := bson.M{"_id": id}
	_ = json.NewDecoder(r.Body).Decode(&wine)

	update := bson.D{
		{"$set", bson.D{
			{"country", wine.Country},
			{"region", wine.Region},
			{"lage", wine.Lage},
			{"sweetness", wine.Sweetness},
			{"sugarLevel", wine.SugarLevel},
			{"wineType", wine.WineType},
			{"wineColor", wine.WineColor},
			{"Title", wine.Title},
			{"description", wine.Description},
			{"alcoholLevel", wine.AlcoholLevel},
			{"vintage", wine.Vintage},
			{"validEAN", wine.ValidEAN},
			{"acidity", wine.Acidity},
			{"winery", wine.Winery},
			{"grape", wine.Grape},
			{"appellation", wine.Appellation},
		}},
	}

	err := collection.FindOneAndUpdate(context.TODO(), filter, update).Decode(&wine)

	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJson(w, http.StatusOK, map[string]string{"result": wine.Title + " was updated!"})
}

func deleteWine(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var params = mux.Vars(r)
	id, err := primitive.ObjectIDFromHex(params["id"])

	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	collection := helpers.ConnectDB()
	filter := bson.M{"_id": id}

	deleteResult, err := collection.DeleteOne(context.TODO(), filter)

	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJson(w, http.StatusOK, deleteResult)
}
