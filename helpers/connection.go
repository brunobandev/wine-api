package helpers

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// ConnectDB is reponsible to return a collection.
func ConnectDB() *mongo.Collection {
	clientOptions := options.Client().ApplyURI("mongodb://grapecheck-datastore")
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	collection := client.Database("grapecheck").Collection("wines")

	return collection
}
