FROM golang:1.14

RUN go get -u github.com/pilu/fresh
RUN go get -u github.com/gorilla/mux
RUN go get -u go.mongodb.org/mongo-driver/mongo

WORKDIR /go/src/github.com/brunobandev/grapecheck

EXPOSE 8080

ENTRYPOINT ["fresh"]