package models

import "go.mongodb.org/mongo-driver/bson/primitive"

// Wine structure
type Wine struct {
	ID           primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Country      string             `json:"country" bson:"country"`
	Region       string             `json:"region" bson:"region"`
	Lage         string             `json:"lage" bson:"lage"`
	Sweetness    string             `json:"sweetness" bson:"sweetness"`
	SugarLevel   string             `json:"sugarLevel" bson:"sugarLevel"`
	WineType     string             `json:"wineType" bson:"wineType"`
	WineColor    string             `json:"wineColor" bson:"wineColor"`
	Title        string             `json:"title" bson:"title"`
	Description  string             `json:"description" bson:"description"`
	AlcoholLevel string             `json:"alcoholLevel" bson:"alcoholLevel"`
	Vintage      string             `json:"vintage" bson:"vintage"`
	ValidEAN     bool               `json:"validEAN" bson:"validEAN"`
	Acidity      string             `json:"acidity" bson:"acidity"`
	Winery       string             `json:"winery" bson:"winery"`
	Grape        string             `json:"grape" bson:"grape"`
	Appellation  string             `json:"appellation" bson:"appellation"`
}
