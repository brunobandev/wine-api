package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	rv1 := r.PathPrefix("/api/v1/").Subrouter()

	rv1.HandleFunc("/wines", getWines).Methods("GET")
	rv1.HandleFunc("/wines/{id}", getWine).Methods("GET")
	rv1.HandleFunc("/wines", createWine).Methods("POST")
	rv1.HandleFunc("/wines/{id}", updateWine).Methods("PUT")
	rv1.HandleFunc("/wines/{id}", deleteWine).Methods("DELETE")

	var port = ":8080"
	fmt.Println("Server running in port:", port)
	log.Fatal(http.ListenAndServe(port, rv1))
}

func respondWithError(w http.ResponseWriter, code int, msg string) {
	respondWithJson(w, code, map[string]string{"error": msg})
}

func respondWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
