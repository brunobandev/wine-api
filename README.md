
# Grapecheck - Wine API v1

The task should give an overview of how you solve a given problem, how you stick to best
practices and given coding standards. How you solve the given task is up to you. It should fit
the task.

## Getting Started  

In the end you should provide us with an application which serves an API which provides the
ability to do the following tasks:

* Get a single wine
* Update a single wine
* Create a single wine
* Delete a single wine
* Get multiple wines
* Delete multiple wines

### Prerequisites

* [Docker](https://docs.docker.com/get-docker/)
* [Docker Compose](https://docs.docker.com/compose/install/)

### Stack

* Golang
* MongoDB
* Docker

### Installing

* Clone the project: `git clone git@bitbucket.org:brunobandev/wine-api.git`
* Open the folder: `cd wine-api`
* Run: `docker-compose up`

### How to use

* Local Access: http://localhost:8080/
* Online Access: http://46.101.232.240:8080/

1. You can check the online documentation [**here**](https://documenter.getpostman.com/view/7099122/T1DpBcnQ?version=latest).
2. Or you can open in your favorite API Client.

#### To do list

- Create an environment variable file.
- Create an best environment for production.
- Separate responsibilities, because inside the handles I have queries
- Add middlewares and CORS.
- Validation of input and output data
- Add cache layers.
- Automated testing
- CI/CD pipeline
- Documentation improvements